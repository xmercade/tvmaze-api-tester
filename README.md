TVMaze API Tester
===================

![Devices](https://img.shields.io/badge/Devices-Univesal-blue)
![Language](https://img.shields.io/badge/Language-Swift-orange)


Master/Detail app to list TVMaze shows
======================================

This tester app uses the TVMaze API to display a list of shows and its details.
For each show, its image and title are shown in the list. 
In the detail, also the show rating and a summary are displayed. 

The list is paginated using an "infinite scroll".
There is also a textField which its purpose is to allow to select a certain page to list. 

API endpoint used:

* URL: /shows?page=:num
* Example: http://api.tvmaze.com/shows?page=1

Architecture
------------

The app is built following the VIPER design pattern.

* View 			— Responsibile to send user actions to the presenter and show whatever the presenter asks it to.
* Interactor 	— It has the business logic for the app specified by a use case.
* Presenter		— Contains the view logic for preparing content for display and for reacting to user interactions.
* Entity 		— The basic model for objects used by the module.
* Router		— Contains logic for routing of screens.

It contains two modules ShowList and ShowDetail, and each module contains the necessary classes to agree the VIPER pattern.
