//
//  UIImage+Utils.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 26/1/21.
//

import UIKit

extension UIImageView
{
    /**
     Extension to download asynchronously an image from a given URL.
     */
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit)
    {
        //  Set default image and content mode
        image = UIImage.init(named:"empty")
        contentMode = mode
        
        //  Start URL session to retrieve the image
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            
            //  Once finished, set the image retrieved in main queue
            DispatchQueue.main.async()
            {
                [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    /**
     Extension to download asynchronously an image from a given URL.
     Added guard to check the URL validity.
     */
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit)
    {
        guard let url = URL(string: link) else
        {
            self.image = UIImage.init(named:"empty")
            return
        }
        downloaded(from: url, contentMode: mode)
    }
}
