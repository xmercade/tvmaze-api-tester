//
//  ShowListInteractor.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import Foundation
import UIKit

/**
 Presenter > Interactor protocol to retrieve the list of shows.
 Presenter will call these methods when receives the request from the view.
 */
protocol ShowListInteractorInputProtocol: AnyObject
{
    var presenter:ShowListInteractorOutputProtocol? {get set}

    func getShowList(forPage page:Int, appending:Bool)
}

/**
 Interactor > Presenter protocol to notify back the presenter when the actions are completed.
 Interactor will call these methods to allow presenter to keep updated the view.
 */
protocol ShowListInteractorOutputProtocol: AnyObject
{
    func showListWillFetch()
    func showListDidFetch(showList:[Show], appending:Bool)
    func fetchDidFail()
}

/**
 The interactor of the list module.
 This class is the responsible to perform all the actions related to the business logic.
 */
class ShowListInteractor : ShowListInteractorInputProtocol
{
    //  MARK: Properties
    //  Reference to notify back the presenter
    var presenter:ShowListInteractorOutputProtocol?
    
    //  MARK: Protocol methods
    /**
     This method launches the fetch call and handles it result to create the entities and notify the presenter.
     */
    func getShowList(forPage page:Int, appending:Bool)
    {
        var showList = [Show]()
        
        //  Call the fetch and manage the result after completion
        fetchShows(forPage: page) { (shows) in
            for show in shows
            {
                let images = show["image"] as? Dictionary<String, String>
                let name = show["name"] as? String
                let rating = show["rating"] as? Dictionary<String, CGFloat>
                let summary = show["summary"] as? String
                
                let show = Show(thumbnail: images?["medium"] ?? "",
                                image: images?["original"] ?? "",
                                name: name ?? "N/A",
                                rating: rating?["average"] ?? 0.0,
                                summary: summary ?? "No summary stored yet")

                showList.append(show)
            }
            
            //  Notify the presenter that the fetch has finished
            self.presenter?.showListDidFetch(showList:showList, appending:appending)
        }
    }
    
    //  MARK: Helpers
    /**
     This method executes the API call asynchronously and when finished, return the dictionary with the JSON results.
     */
    func fetchShows(forPage page:Int, withCompletion completion: @escaping ([[String:Any]]) -> ())
    {
        //  Notify the presenter to update the view status (starts the loading)
        presenter?.showListWillFetch()
        
        //  Dispatch the API request in a new separate queue
        DispatchQueue.global().async
        {
            do
            {
                let url = URL(string:"http://api.tvmaze.com/shows?page=\(page)")
                let data = try Data(contentsOf: url!)
                
                //  Parse the JSON response and create the result dictionary
                let jsonArray = try JSONSerialization.jsonObject(with: data) as! [[String:Any]]
                
                //  Force to handle the result in the main queue as it requires view updates
                DispatchQueue.main.async
                {
                    completion(jsonArray)
                }
            }
            catch
            {
                //  In case of error, also force to handle in the main queue and notify the presenter to update the view accordingly
                DispatchQueue.main.async
                {
                    self.presenter?.fetchDidFail()
                    print("Unable to get show list")
                }                
            }
        }
    }
}
