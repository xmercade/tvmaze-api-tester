//
//  ShowListRouter.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit

/**
 The router protocol that published the methods available to redirect to another module.
 Presenter will call these methods when a redirect is needed after an user action.
 */
protocol ShowListRouterProtocol: AnyObject
{
    func pushShowDetail(with show:Show, from view:UIViewController)
}

/**
 The router of the list module.
 This class handles the redirections from this module to any other.
 */
class ShowListRouter : ShowListRouterProtocol
{
    /**
     Static initializer to setup the module links.
     View will call the static initializer to setup the module links.
     */
    class func createShowListModule(showListView:ShowListView)
    {
        //  Presenter reference with protocols to interact with the view in both directions
        let presenter:ShowListPresenterProtocol & ShowListInteractorOutputProtocol = ShowListPresenter()
        
        //  Setup the links
        showListView.presenter = presenter
        showListView.presenter?.router = ShowListRouter()
        showListView.presenter?.view = showListView
        showListView.presenter?.interactor = ShowListInteractor()
        showListView.presenter?.interactor?.presenter = presenter
    }
    
    //  MARK: Protocol methods
    /**
     Creates the new module and pushes the detail of a show in the navigationController.
     */
    func pushShowDetail(with show:Show, from view:UIViewController)
    {
        //  Get the detailView reference and initialize the new module router with it
        let showDetailVC = view.storyboard?.instantiateViewController(withIdentifier:"ShowDetailView") as! ShowDetailView
        ShowDetailRouter.createShowDetailModule(with:showDetailVC, and:show)
        
        //  Push the detailView
        view.navigationController?.pushViewController(showDetailVC, animated:true)
    }
}
