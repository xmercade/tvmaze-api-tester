//
//  ShowListView.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit

/**
 View protocol to publish display, append and fail functions as well as loading boolean.
 Presenter will call these methods to apply changes on the view.
 */
protocol ShowListViewProtocol: AnyObject
{
    var isLoading:Bool {get set}
    
    func displayShows(with shows:[Show])
    func appendShows(with shows:[Show])
    func fetchFailed()
}

/**
 The view of the list module.
 This class has the references of the elements in the view and handles its primaries behaviours.
 */
class ShowListView : UIViewController, ShowListViewProtocol
{
    //  MARK: Constants
    static let defaultPage: Int = 1
    static let cellHeight: CGFloat = 80.0
    
    //  MARK: View outlets
    @IBOutlet weak var invalidPageView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingPageLabel: UILabel!
    @IBOutlet weak var showTableView:UITableView!
    @IBOutlet weak var pageTextField: UITextField!
    @IBOutlet weak var summaryLabel: UILabel!
    
    //  MARK: Properties
    //  Reference of the presenter to communicate user actions to it
    var presenter:ShowListPresenterProtocol?
    
    var showList = [Show]()
    var startPage:Int = ShowListView.defaultPage
    var endPage:Int = ShowListView.defaultPage
    var selectedPage:Int = ShowListView.defaultPage
    var _isLoading:Bool = false
    
    var isLoading:Bool
    {
        get { return _isLoading }
        
        //  Use the loading setter to handle the loading view update
        set
        {
            _isLoading = newValue
            loadingPageLabel.text = "Loading page \(selectedPage)..."
            loadingView.isHidden = !_isLoading
        }
    }
    
    //  MARK: Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //  Setup delegates and tableView datasource
        pageTextField.delegate = self
        showTableView.delegate = self
        showTableView.dataSource = self
        
        //  Initialization
        summaryLabel.text = "Showing results for page \(startPage)"
        isLoading = false
        
        //  Call static router initializer to link all members of the ShowList module
        ShowListRouter.createShowListModule(showListView:self)
        
        //  Load the data
        presentResults(appending:false)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    //  MARK: Protocol methods
    /**
     Replace the existing showList array and reloads the tableView.
     Updates the startPage, endPage and the summary label at the end.
     */
    func displayShows(with shows:[Show])
    {
        showList = shows
        showTableView.reloadData()
        startPage = selectedPage
        endPage = selectedPage
        updateSummary()
    }
    
    /**
     Appends to the existing showList array the fetched shows and reloads the tableView.
     Updates only the endPage and the summary label at the end.
     */
    func appendShows(with shows:[Show])
    {
        showList.append(contentsOf:shows)
        showTableView.reloadData()
        endPage = selectedPage
        updateSummary()
    }
    
    /**
     Updates the control values in case of a failed fetch call, and shows the invalidPageView.
     */
    func fetchFailed()
    {
        updateSummary()
        
        isLoading = false
        invalidPageView.isHidden = false
    }
    
    //  MARK: Helpers
    /**
     Starts a new fetch call.
     First hides any possible error displayed and then tells the presenter to start the call.
     */
    func presentResults(appending:Bool)
    {
        invalidPageView.isHidden = true
        presenter?.performFetchCall(forPage:selectedPage, appending:appending)
    }

    /**
     Scrolls the tableView to its first row.
     */
    func scrollToFirstRow()
    {
        let indexPath = NSIndexPath(row: 0, section: 0) as IndexPath
        showTableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    /**
     Updates the summary label with the current state values.
     */
    func updateSummary()
    {
        let summary = (startPage == endPage) ? "for page \(startPage)" : "from page \(startPage) to page \(endPage)"
        
        summaryLabel.text = "Showing results " + summary
    }
}

/**
 Extension to handle the tableView delegate and dataSource.
 */
extension ShowListView : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ShowListView.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return showList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //  Dequeue the cell and unwrap it as ShowTableViewCell
        let cell = showTableView.dequeueReusableCell(withIdentifier:"showCell", for: indexPath) as! ShowTableViewCell
        let show = showList[indexPath.row]
        
        //  Setup the cell with the entity values
        cell.showName.text = "\(show.name ?? "N/A")"
        cell.showThumbnail.downloaded(from:show.thumbnail!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        presenter?.displaySelectedShow(with:showList[indexPath.row], from: self)
    }
    
    /**
     Handles the "infinite scroll" behaviour.
     Once the scroll reaches the end of its view, it launches a fetch call to append the results to the existing ones.
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset
        
        //  Only launch a new fetch call if there isn't another call loading and if the last call hasn't failed
        if distanceFromBottom < height && !self.isLoading && self.invalidPageView.isHidden
        {
            self.selectedPage+=1
            self.presentResults(appending:true)
        }
    }
}

/**
 Extension to handle the textField deleagte.
 */
extension ShowListView : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        //  Once return key is pressed, update the startPage with the textField contents
        selectedPage = Int(textField.text!) ?? ShowListView.defaultPage
        self.scrollToFirstRow()
      
        //  Launch a fetch call to get the selected page results
        presentResults(appending:false)        
        
        return true
    }
}
