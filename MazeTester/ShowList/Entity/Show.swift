//
//  Show.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import Foundation
import UIKit

/**
 The entity of the list and detail modules.
 This class has the description of the model used to display a show.
 */
class Show
{
    //  MARK: Properties
    var thumbnail: String?
    var image: String?
    var name: String?
    var rating: CGFloat?
    var summary: String?
    
    //  MARK: Initializer
    init(thumbnail:String, image:String, name:String, rating:CGFloat, summary:String)
    {
        self.thumbnail = thumbnail
        self.image = image
        self.name = name
        self.rating = rating
        self.summary = prepareHTML(with:summary)
    }
    
    //  MARK: Helpers
    /**
     Prepares the summary text to be properly displayed in a WKWebView.
     */
    func prepareHTML(with summary:String) -> String
    {
        return """
        <html>
            <head>
                <meta name='viewport' content='initial-scale=1.0'/>
                <style>
                    body {
                        background-color: transparent;
                        font-family: Verdana, Arial, Helvetica, sans-serif;
                        color: black;
                        font-size: 16px;
                        padding:0;
                        margin:0;
                    }
                </style>
            </head>
            <body>
            \(summary)
            </body>
        </html>
        """
    }
}
