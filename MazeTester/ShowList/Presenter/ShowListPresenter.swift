//
//  ShowListPresenter.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit

/**
 Presenter protocol to publish actions that the view can request to the presenter.
 View will call these methods when the user interacts with it.
 */
protocol ShowListPresenterProtocol: AnyObject
{
    var interactor:ShowListInteractorInputProtocol? {get set}
    var view:ShowListViewProtocol? {get set}
    var router:ShowListRouterProtocol? {get set}

    func performFetchCall(forPage page:Int, appending:Bool)
    func displaySelectedShow(with show:Show, from view:UIViewController)
}

/**
 The presenter of the list module.
 This class manages the communication from and to the view.
 */
class ShowListPresenter : ShowListPresenterProtocol
{
    //  MARK: Properties
    //  Reference of the router of the module to redirect to another modules
    var router:ShowListRouterProtocol?
    //  Reference of the view to communicate the updates
    weak var view:ShowListViewProtocol?
    //  Reference of the interactor to request user actions
    var interactor:ShowListInteractorInputProtocol?
    
    //  MARK: Protocol methods (from the view)
    /**
     This method notifies the router to redirect to the detail of the show once the user has selected a show in the tableView.
     */
    func displaySelectedShow(with show:Show, from view:UIViewController)
    {
        router?.pushShowDetail(with:show, from:view)
    }
    
    /**
     Request a new fetch of data.
     The request has to be done for the given page and taking into account if the mode is ovewrite or append.
     */
    func performFetchCall(forPage page:Int, appending:Bool)
    {
        interactor?.getShowList(forPage:page, appending:appending)
    }
}

/**
 Extension to handle the protocol methods to notify back the view.
 */
extension ShowListPresenter : ShowListInteractorOutputProtocol
{
    //  MARK: Protocol methods (to the view)
    /**
     Notifies the view that an error has occurred during the fetch.
     */
    func fetchDidFail()
    {
        view?.fetchFailed()
    }
    
    /**
     Notifies the view that the fetch has started.
     */
    func showListWillFetch()
    {
        view?.isLoading = true
    }
    
    /**
     Calls the required view method to update it with the results taking into account the mode.
     */
    func showListDidFetch(showList:[Show], appending:Bool)
    {
        view?.isLoading = false
        
        if appending { view?.appendShows(with:showList) }
        else { view?.displayShows(with:showList) }
    }
}
