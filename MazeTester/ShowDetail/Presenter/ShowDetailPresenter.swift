//
//  ShowDetailPresenter.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit

/**
 Presenter protocol to publish actions that the view can request to the presenter.
 View will call these methods when the user interacts with it.
 */
protocol ShowDetailPresenterProtocol: AnyObject
{
    var view: ShowDetailViewProtocol? {get set}
    var router: ShowDetailRouterProtocol? {get set}
 
    func viewDidLoad()
    func backButtonAction(from view:UIViewController)
}

/**
 The presenter of the detail module.
 This class manages the communication from and to the view.
 */
class ShowDetailPresenter : ShowDetailPresenterProtocol
{
    //  MARK: Properties
    //  Reference of the router of the module to redirect to another modules
    var router: ShowDetailRouterProtocol?
    //  Reference of the view to communicate the updates
    weak var view: ShowDetailViewProtocol?
   
    var show: Show?
    
    /**
     Tell the view to configure its contents with the values of the given show.
     */
    func viewDidLoad()
    {
        view?.displayShowDetail(with:show!)
    }
    
    /**
     This method notifies the router to redirect back to the show list.
     */
    func backButtonAction(from view:UIViewController)
    {
        router?.returnToShowListView(from:view)
    }
}
