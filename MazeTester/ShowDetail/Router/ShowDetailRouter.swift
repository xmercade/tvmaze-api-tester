//
//  ShowDetailRouter.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit

/**
 The router protocol that published the methods available to redirect to another module.
 Presenter will call these methods when a redirect is needed after an user action.
 */
protocol ShowDetailRouterProtocol: AnyObject
{
    func returnToShowListView(from view:UIViewController)
}

/**
 The router of the detail module.
 This class handles the redirections from this module to any other.
 */
class ShowDetailRouter : ShowDetailRouterProtocol
{
    /**
     Static initializer to setup the module links.
     */
    class func createShowDetailModule(with showDetailView:ShowDetailView, and show:Show)
    {
        let presenter = ShowDetailPresenter()
        presenter.show = show
        showDetailView.presenter = presenter
        showDetailView.presenter?.view = showDetailView
        showDetailView.presenter?.router = ShowDetailRouter()
    }
    
    //  MARK: Protocol methods
    /**
     Gets the view navigationController to call the pop method and go back to the show list.
     */
    func returnToShowListView(from view: UIViewController)
    {
        view.navigationController?.popViewController(animated:true)
    }
}

