//
//  ShowDetailView.swift
//  MazeTester
//
//  Created by Xavier Mercadé on 25/1/21.
//

import UIKit
import WebKit

/**
 View protocol to publish the display of a show detail.
 Presenter will call these methods to apply changes on the view.
 */
protocol ShowDetailViewProtocol: AnyObject
{
    func displayShowDetail(with show:Show)
}

/**
 The view of the detail module.
 This class has the references of the elements in the view and handles its primaries behaviours.
 */
class ShowDetailView : UIViewController, ShowDetailViewProtocol
{
    //  MARK: Constants
    static let headerHeight: CGFloat = 100.0
    
    //  MARK: View outlets
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showRating: UILabel!
    @IBOutlet weak var showSummaryWebView: WKWebView!
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet var contentViewHeight: NSLayoutConstraint!
    
    //  MARK: Properties
    //  Reference of the presenter to communicate user actions to it.
    var presenter:ShowDetailPresenterProtocol?
    
    //  MARK: Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //  Add blur effect to the headerView and the contentView
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        
        let blurEffectHeaderView = UIVisualEffectView(effect: blurEffect)
        blurEffectHeaderView.frame = headerView.bounds
        blurEffectHeaderView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        headerView.backgroundColor = .clear
        headerView.insertSubview(blurEffectHeaderView, at: 0)
        
        let blurEffectContentView = UIVisualEffectView(effect: blurEffect)
        blurEffectContentView.frame = contentView.bounds
        blurEffectContentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = .clear
        contentView.insertSubview(blurEffectContentView, at: 0)
        
        //  Configure the WKWebView
        showSummaryWebView.isOpaque = false
        showSummaryWebView.backgroundColor = .clear
        showSummaryWebView.scrollView.backgroundColor = .clear
        
        //  Setup actionButton states and images associated
        actionButton.setImage(UIImage(named: "expand"), for:.normal)
        actionButton.setImage(UIImage(named: "collapse"), for:.selected)
        
        presenter?.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //  MARK: Protocol methods
    /**
     Configures the view with the given show information.
     */
    func displayShowDetail(with show:Show)
    {
        showName.text = "\(show.name ?? "N/A")"
        showImage.downloaded(from:show.image!)
        showRating.text = "Rating: \(show.rating ?? 0.0)"
        showSummaryWebView.loadHTMLString(show.summary!, baseURL:Bundle.main.bundleURL)
    }
    
    //  MARK: Helpers
    /**
     Animates the headerView and contentView when actionButton is tapped.
     */
    func animateViewConstraints()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            self.contentView.isHidden = self.contentViewHeight.isActive ? false : true
        })
    }
    
    //  MARK: Buttons actions
    /**
     Action to return to the show list.
     */
    @IBAction func backButtonAction(_ sender: UIButton)
    {
        presenter?.backButtonAction(from:self)
    }

    /**
     Action to show or hide the headerView and contentView to better display the show image.
     */
    @IBAction func actionButtonAction(_ sender: UIButton)
    {
        //  Update the constraints accordingly to the action (expand/collapse)
        headerViewHeight.constant = !sender.isSelected ? 0 : ShowDetailView.headerHeight
        contentViewHeight.isActive = !sender.isSelected ? false : true
        
        if sender.isSelected { contentView.isHidden = false }
        
        //  Animate the view to apply the constraints changes
        animateViewConstraints()
        
        sender.isSelected = !sender.isSelected
    }
}
